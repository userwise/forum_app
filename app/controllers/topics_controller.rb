class TopicsController < ApplicationController
  def index
    @topics = Topic.all
  end

  def create
    @topic = Topic.new(topic_params)

    if @topic.save
      flash[:notice] = "Topic created"
      redirect_to topics_path
    else
      flash.now[:alert] = "Topic could not be saved, try again"
      render :new
    end
  end

  def new
    @topic = Topic.new
  end

  def destroy
  end

  def show
  end

  private

  def topic_params
    params.require(:topic).permit(:title, :body)
  end
end
