Rails.application.routes.draw do

  resources :topics, except: [:edit, :update]

  get 'sign_in', to: 'sessions#new'
  post 'sign_in', to: 'sessions#create'
  delete 'sign_out', to: 'sessions#destroy'

  root 'home#index'
end
